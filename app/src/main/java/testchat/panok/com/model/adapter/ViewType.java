package testchat.panok.com.model.adapter;

public interface ViewType {

    int getViewType();
}