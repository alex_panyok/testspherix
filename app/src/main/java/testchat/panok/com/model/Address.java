package testchat.panok.com.model;

import java.io.Serializable;

public class Address implements Serializable {

    private String host;

    private int port;

    public Address(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }
}
