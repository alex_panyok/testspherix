package testchat.panok.com.model.adapter;

import testchat.panok.com.model.TextMessage;
import testchat.panok.com.utils.AdapterKeys;

final public class MeTextMessageItem extends TextMessageItem {


    public MeTextMessageItem(TextMessage textMessage) {
        super(textMessage);
    }

    @Override
    public int getViewType() {
        return AdapterKeys.ME_MESSAGE_TYPE;
    }
}
