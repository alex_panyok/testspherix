package testchat.panok.com.model.adapter;

import testchat.panok.com.model.TextMessage;

public abstract class TextMessageItem implements ViewType {

    private TextMessage textMessage;

    TextMessageItem(TextMessage textMessage) {
        this.textMessage = textMessage;
    }

    public TextMessage getTextMessage() {
        return textMessage;
    }

}
