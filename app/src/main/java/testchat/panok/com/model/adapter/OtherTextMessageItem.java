package testchat.panok.com.model.adapter;

import testchat.panok.com.model.TextMessage;
import testchat.panok.com.utils.AdapterKeys;

final public class OtherTextMessageItem extends TextMessageItem {


    public OtherTextMessageItem(TextMessage textMessage) {
        super(textMessage);
    }

    @Override
    public int getViewType() {
        return AdapterKeys.OTHER_MESSAGE_TYPE;
    }
}
