package testchat.panok.com.model;

import java.io.Serializable;

import testchat.panok.com.security.TEA;

public class TextMessage implements Serializable {

    private String text;

    private byte[] textBytes;

    public TextMessage(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void decrypt(TEA tea) {
        if (textBytes != null && textBytes.length>0) {
            text = new String(tea.decrypt(textBytes));
            textBytes = null;
        }
    }


    public void encrypt(TEA tea) {
        if (text != null && !text.isEmpty()) {
            textBytes = tea.encrypt(text.getBytes());
            text = null;
        }
    }

}
