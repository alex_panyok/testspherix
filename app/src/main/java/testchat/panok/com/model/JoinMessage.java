package testchat.panok.com.model;

import java.io.Serializable;

import testchat.panok.com.enums.SideType;

public class JoinMessage implements Serializable {

    public static final int JOINED = 1;

    public static final int REJECTED = 2;

    private String encryptedKey;

    private int status;

    private SideType sideType;

    public JoinMessage(SideType sideType,int status, String encryptedKey) {
        this.encryptedKey = encryptedKey;
        this.status = status;
        this.sideType = sideType;
    }

    public JoinMessage(SideType sideType, int status) {
        this.status = status;
        this.sideType = sideType;
    }

    public String getEncryptedMessage() {
        return encryptedKey;
    }

    public void setEncryptedMessage(String encryptedMessage) {
        this.encryptedKey = encryptedMessage;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public SideType getSideType() {
        return sideType;
    }
}
