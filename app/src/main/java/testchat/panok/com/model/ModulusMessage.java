package testchat.panok.com.model;

import java.io.Serializable;

public class ModulusMessage implements Serializable{

    private String modulus;


    public ModulusMessage(String modulus) {
        this.modulus = modulus;
    }

    public String getModulus() {
        return modulus;
    }
}
