package testchat.panok.com.utils;

final public class AdapterKeys {

    public static final int ME_MESSAGE_TYPE = 1001;

    public static final int OTHER_MESSAGE_TYPE = 1002;
}
