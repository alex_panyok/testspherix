package testchat.panok.com.utils;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;

final public class ConnectivityUtils {

    public static String getIp(Context context){
        WifiManager wm = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wm != null)
            return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        else
            return null;
    }
}
