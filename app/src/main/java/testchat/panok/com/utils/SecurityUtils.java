package testchat.panok.com.utils;

import java.util.UUID;

final public class SecurityUtils {

    public static String generatePrivateKey() {
        return UUID.randomUUID().toString();
    }
}
