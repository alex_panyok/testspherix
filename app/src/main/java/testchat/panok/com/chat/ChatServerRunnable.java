package testchat.panok.com.chat;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;

import testchat.panok.com.chat.callback.ChatRunnableCallback;
import testchat.panok.com.enums.State;
import testchat.panok.com.enums.SideType;
import testchat.panok.com.model.JoinMessage;
import testchat.panok.com.model.ModulusMessage;
import testchat.panok.com.utils.SecurityUtils;


final public class ChatServerRunnable extends ChatRunnable {

    private ServerSocket serverSocket;


    ChatServerRunnable(ChatRunnableCallback chatRunnableCallback, ExecutorService executorService, String publicKey) {
        super(chatRunnableCallback, executorService, publicKey);
    }


    @Override
    protected void onNewMessage(Object message, boolean me) {
        super.onNewMessage(message, me);
        if (message instanceof JoinMessage) {
            JoinMessage joinMessage = (JoinMessage) message;
            if (chatRunnableCallbackWeakReference.get() != null)
                chatRunnableCallbackWeakReference.get().onJoinMessage(joinMessage);
        }
        if (message instanceof ModulusMessage) {
            ModulusMessage modulusMessage = (ModulusMessage) message;
            modulus = modulusMessage.getModulus();
            writeMessage(new JoinMessage(SideType.SERVER, JoinMessage.JOINED, rsa.encrypt(generateKeyAndWrite(), modulus, publicKey)));
        }
    }


    @Override
    void init() {
        try {
            runNewChatServer();
        } catch (Exception e) {
            sendDisconnect(e);
            connected = false;
            e.printStackTrace();
        }
    }

    private String generateKeyAndWrite() {
        privateKey = SecurityUtils.generatePrivateKey();
        initTEA(privateKey);
        return privateKey;
    }


    private void runNewChatServer() throws Exception {
        serverSocket = new ServerSocket(ChatService.PORT);
        if (chatRunnableCallbackWeakReference.get() != null)
            chatRunnableCallbackWeakReference.get().onServerChangeState(State.STARTED);
        socket = serverSocket.accept();
        objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        objectInputStream = new ObjectInputStream(socket.getInputStream());
        connected = true;
    }

    @Override
    public void disconnect() {
        super.disconnect();
        try {
            serverSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        serverSocket = null;
    }
}
