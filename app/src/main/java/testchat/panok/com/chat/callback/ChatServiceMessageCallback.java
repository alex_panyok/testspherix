package testchat.panok.com.chat.callback;

import testchat.panok.com.model.TextMessage;

public interface ChatServiceMessageCallback extends ChatServiceCallback {

    void textMessage(TextMessage message);

    void meTextMessage(TextMessage message);
}
