package testchat.panok.com.chat.callback;

import testchat.panok.com.enums.State;
import testchat.panok.com.model.JoinMessage;
import testchat.panok.com.model.TextMessage;

public interface ChatRunnableCallback {

    void onServerChangeState(State serverState);

    void onClientStateChange(State serverState);

    void onNewMessage(TextMessage textMessage);

    void onMeNewMessage(TextMessage textMessage);

    void onJoinMessage(JoinMessage joinMessage);

    void onDisconnect(Throwable cause);
}
