package testchat.panok.com.chat;

import testchat.panok.com.chat.callback.ChatServiceCallback;

public interface ChatService {

    String ACTION_SERVER = "ACTION_SERVER";

    String ACTION_CLIENT = "ACTION_CLIENT";

    int PORT = 9001;

    void sendTextMessage(String message);

    void addCallback(ChatServiceCallback chatServiceCallback);

    void removeCallback(ChatServiceCallback chatServiceCallback);

    void removeAllCallbacks();

    void disconnect();
}
