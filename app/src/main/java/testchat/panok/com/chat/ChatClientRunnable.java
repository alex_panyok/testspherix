package testchat.panok.com.chat;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

import testchat.panok.com.chat.callback.ChatRunnableCallback;
import testchat.panok.com.enums.SideType;
import testchat.panok.com.enums.State;
import testchat.panok.com.model.JoinMessage;
import testchat.panok.com.model.ModulusMessage;

final public class ChatClientRunnable extends ChatRunnable {

    private static final int CONNECT_TIMEOUT = 500;

    private String host;

    private int port;

    ChatClientRunnable(ChatRunnableCallback chatRunnableCallback, ExecutorService executorService, String publicKey, String host, int port) {
        super(chatRunnableCallback, executorService, publicKey);
        this.host = host;
        this.port = port;
    }

    @Override
    void init() {
        try {
            tryToConnect(host, port);
        } catch (Exception e) {
            sendDisconnect(e);
            connected = false;
            e.printStackTrace();
        }
    }

    private void tryToConnect(String host, int port) throws Exception {
        socket = new Socket();
        socket.connect(new InetSocketAddress(host, port), CONNECT_TIMEOUT);
        objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        objectInputStream = new ObjectInputStream(socket.getInputStream());
        if (chatRunnableCallbackWeakReference.get() != null)
            chatRunnableCallbackWeakReference.get().onClientStateChange(State.STARTED);
        connected = true;
    }

    @Override
    protected void onNewMessage(Object message, boolean me) {
        super.onNewMessage(message, me);
        if (message instanceof JoinMessage) {
            JoinMessage joinMessage = (JoinMessage) message;
            if (joinMessage.getEncryptedMessage() != null) {
                privateKey = rsa.decrypt(joinMessage.getEncryptedMessage());
                initTEA(privateKey);
            }
            writeMessage(new JoinMessage(SideType.CLIENT, modulus != null ? JoinMessage.JOINED : JoinMessage.REJECTED));
            if (chatRunnableCallbackWeakReference.get() != null)
                chatRunnableCallbackWeakReference.get().onJoinMessage(joinMessage);
        }

        if (message instanceof ModulusMessage) {
            ModulusMessage modulusMessage = (ModulusMessage) message;
            modulus = modulusMessage.getModulus();
        }


    }
}
