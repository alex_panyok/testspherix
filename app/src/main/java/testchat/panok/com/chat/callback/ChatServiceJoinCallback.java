package testchat.panok.com.chat.callback;

import testchat.panok.com.enums.State;
import testchat.panok.com.model.JoinMessage;

public interface ChatServiceJoinCallback extends ChatServiceCallback {

    void join(JoinMessage joinMessage);

    void serverStateChanged(State serverState);

    void clientChangeState(State clientState);
}
