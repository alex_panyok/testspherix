package testchat.panok.com.chat;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

import testchat.panok.com.chat.callback.ChatRunnableCallback;
import testchat.panok.com.model.ModulusMessage;
import testchat.panok.com.model.TextMessage;
import testchat.panok.com.security.RSA;
import testchat.panok.com.security.TEA;

public abstract class ChatRunnable implements Runnable {

    boolean connected = false;

    Socket socket;

    ObjectOutputStream objectOutputStream;

    ObjectInputStream objectInputStream;

    private WeakReference<ExecutorService> executorServiceWeakReference;

    String publicKey;

    String modulus;

    WeakReference<ChatRunnableCallback> chatRunnableCallbackWeakReference;

    String privateKey;

    RSA rsa;

    private TEA tea;

    ChatRunnable(ChatRunnableCallback chatRunnableCallback, ExecutorService executorService, String publicKey) {
        this.executorServiceWeakReference = new WeakReference<>(executorService);
        this.chatRunnableCallbackWeakReference = new WeakReference<>(chatRunnableCallback);
        this.publicKey = publicKey;
    }

    private void initRSA(String publicKey) {
        rsa = new RSA(publicKey);
    }

    void initTEA(String privateKey) {
        tea = new TEA(privateKey.getBytes());
    }

    @Override
    public void run() {
        initRSA(publicKey);
        init();
        if (connected)
            writeMessage(new ModulusMessage(rsa.getModulus()));
        readMessage();
    }

    abstract void init();

    private void readMessage() {
        while (objectInputStream != null && connected)
            try {
                Object o = objectInputStream.readObject();
                if (o instanceof TextMessage) {
                    TextMessage textMessage = (TextMessage) o;
                    textMessage.decrypt(tea);
                    onNewMessage(textMessage, false);
                } else {
                    onNewMessage(o, false);
                }
            } catch (IOException | ClassNotFoundException e) {
                sendDisconnect(e);
                connected = false;
                e.printStackTrace();
            }
    }

    void sendDisconnect(Throwable e) {
        if (chatRunnableCallbackWeakReference.get() != null) {
            chatRunnableCallbackWeakReference.get().onDisconnect(e);

        }
    }


    void writeMessage(final Object object) {
        if (executorServiceWeakReference.get() != null) {
            executorServiceWeakReference.get().submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (object instanceof TextMessage) {
                            TextMessage textMessage = new TextMessage(((TextMessage) object).getText());
                            ((TextMessage) object).encrypt(tea);
                            objectOutputStream.writeObject(object);
                            onNewMessage(textMessage, true);
                        } else
                            objectOutputStream.writeObject(object);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    protected synchronized void onNewMessage(Object message, boolean me) {
        if (message instanceof TextMessage) {
            if (chatRunnableCallbackWeakReference.get() != null) {
                if (!me)
                    chatRunnableCallbackWeakReference.get().onNewMessage((TextMessage) message);
                else
                    chatRunnableCallbackWeakReference.get().onMeNewMessage((TextMessage) message);
            }
        }

    }

    public void disconnect() {
        try {
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        socket = null;
    }


}
