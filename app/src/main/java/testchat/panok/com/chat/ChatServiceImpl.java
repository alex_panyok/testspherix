package testchat.panok.com.chat;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.util.ArraySet;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import testchat.panok.com.R;
import testchat.panok.com.chat.callback.ChatRunnableCallback;
import testchat.panok.com.chat.callback.ChatServiceCallback;
import testchat.panok.com.chat.callback.ChatServiceJoinCallback;
import testchat.panok.com.chat.callback.ChatServiceMessageCallback;
import testchat.panok.com.enums.SideType;
import testchat.panok.com.enums.State;
import testchat.panok.com.model.Address;
import testchat.panok.com.model.JoinMessage;
import testchat.panok.com.model.TextMessage;
import testchat.panok.com.utils.Utils;

final public class ChatServiceImpl extends Service implements ChatService, ChatRunnableCallback {


    public static final String ADDRESS = "ADDRESS";

    private Thread chatThread;

    private ChatRunnable chatRunnable;

    private ExecutorService cachedExecutorService = Executors.newCachedThreadPool();

    private ChatBinder chatBinder = new ChatBinder();

    private State serverState = State.OFF;

    private State clientState = State.OFF;

    private final ArraySet<ChatServiceCallback> callbacks = new ArraySet<>();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startOnAction(intent);
        return START_STICKY;
    }

    private void startOnAction(Intent intent) {
        if (intent.getAction() == null)
            return;
        if (chatRunnable == null) {
            startThread(intent.getAction(), intent);
        } else {
            performAction(intent.getAction());
        }


    }


    private void startThread(String action, Intent intent) {
        switch (action) {
            case ACTION_SERVER:
                chatRunnable = new ChatServerRunnable(this, cachedExecutorService, getResources().getString(R.string.public_key));
                break;

            case ACTION_CLIENT:
                Address address = (Address) intent.getSerializableExtra(ADDRESS);
                chatRunnable = new ChatClientRunnable(this, cachedExecutorService, getResources().getString(R.string.public_key), address.getHost(), address.getPort());
                break;
        }

        if (chatRunnable != null) {
            if (chatThread == null || chatThread.getState() == Thread.State.TERMINATED)
                chatThread = new Thread(chatRunnable);
            if (chatThread.getState() == Thread.State.RUNNABLE || chatThread.getState() == Thread.State.NEW)
                chatThread.start();
        }
    }

    private void performAction(String action) {
        switch (action) {
            case ACTION_SERVER:
                if (serverState == State.STARTED) {
                    disconnect();
                }
                break;

            case ACTION_CLIENT:
                if (clientState == State.STARTED) {
                    disconnect();
                }
                break;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return chatBinder;
    }

    @Override
    public void addCallback(ChatServiceCallback chatServiceCallback) {
        callbacks.add(chatServiceCallback);
    }

    @Override
    public void removeCallback(ChatServiceCallback chatServiceCallback) {
        callbacks.remove(chatServiceCallback);
    }

    @Override
    public void removeAllCallbacks() {
        callbacks.clear();
    }

    @Override
    public void disconnect() {
        release();
        notifyDisconnect();
    }

    private void notifyDisconnect() {
        for (final ChatServiceCallback callback : callbacks) {
            if (callback != null)
                callback.disconnect();
            if (callback instanceof ChatServiceJoinCallback) {
                Utils.invokeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        ((ChatServiceJoinCallback) callback).serverStateChanged(serverState);
                        ((ChatServiceJoinCallback) callback).clientChangeState(clientState);
                    }
                });
            }
        }
    }

    private void notifyJoin(JoinMessage joinMessage) {
        for (ChatServiceCallback callback : callbacks) {
            if (callback instanceof ChatServiceJoinCallback)
                ((ChatServiceJoinCallback) callback).join(joinMessage);
        }
    }

    private void notifyTextMessage(final TextMessage textMessage) {
        for (final ChatServiceCallback callback : callbacks) {
            if (callback instanceof ChatServiceMessageCallback)
                Utils.invokeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        ((ChatServiceMessageCallback) callback).textMessage(textMessage);
                    }
                });

        }
    }

    private void notifyMeTextMessage(final TextMessage textMessage) {
        for (final ChatServiceCallback callback : callbacks) {
            if (callback instanceof ChatServiceMessageCallback)
                Utils.invokeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        ((ChatServiceMessageCallback) callback).meTextMessage(textMessage);
                    }
                });
        }
    }


    private void notifyServerChangeState(final State serverState) {
        for (final ChatServiceCallback callback : callbacks) {
            if (callback instanceof ChatServiceJoinCallback)
                Utils.invokeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        ((ChatServiceJoinCallback) callback).serverStateChanged(serverState);
                    }
                });
        }
    }


    private void notifyClientChangeState(final State clientState) {
        for (final ChatServiceCallback callback : callbacks) {
            if (callback instanceof ChatServiceJoinCallback)
                Utils.invokeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        ((ChatServiceJoinCallback) callback).clientChangeState(clientState);
                    }
                });
        }
    }


    @Override
    public void onDestroy() {
        release();
        super.onDestroy();
    }

    private void release() {
        if (chatRunnable != null)
            chatRunnable.disconnect();
        serverState = State.OFF;
        clientState = State.OFF;
        chatRunnable = null;
        chatThread = null;
    }


    @Override
    public void onServerChangeState(State serverState) {
        this.serverState = serverState;
        notifyServerChangeState(serverState);
    }

    @Override
    public void onClientStateChange(State clientState) {
        this.clientState = clientState;
        notifyClientChangeState(clientState);
    }

    @Override
    public void onNewMessage(TextMessage textMessage) {
        notifyTextMessage(textMessage);
    }

    @Override
    public void onMeNewMessage(TextMessage textMessage) {
        notifyMeTextMessage(textMessage);
    }

    @Override
    public void onJoinMessage(JoinMessage joinMessage) {
        notifyJoin(joinMessage);

    }

    @Override
    public void onDisconnect(Throwable cause) {
        release();
        notifyDisconnect();

    }

    @Override
    public void sendTextMessage(String message) {
        chatRunnable.writeMessage(new TextMessage(message));
    }


    public class ChatBinder extends Binder {
        public ChatServiceImpl getService() {
            return ChatServiceImpl.this;
        }
    }
}
