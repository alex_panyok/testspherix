package testchat.panok.com.view.adapter.delegate.holder;

import android.view.ViewGroup;

import testchat.panok.com.R;

final public class OtherTextMessageHolder extends TextMessageHolder {

    public OtherTextMessageHolder(ViewGroup parent) {
        super(parent, R.layout.item_other_message);
    }
}
