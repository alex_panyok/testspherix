package testchat.panok.com.view.adapter.delegate;

import testchat.panok.com.model.adapter.TextMessageItem;
import testchat.panok.com.view.adapter.delegate.holder.TextMessageHolder;

public abstract class TextMessageDelegateAdapter<I extends TextMessageItem, VH extends TextMessageHolder> implements ViewTypeDelegateAdapter<I, VH> {

    @Override
    public void onBindViewHolder(VH holder, I item) {
        holder.bind(item);
    }
}
