package testchat.panok.com.view.adapter;

import testchat.panok.com.model.adapter.TextMessageItem;
import testchat.panok.com.utils.AdapterKeys;
import testchat.panok.com.view.adapter.delegate.MeMessageDelegateAdapter;
import testchat.panok.com.view.adapter.delegate.OtherMessageDelegateAdapter;

final public class ChatAdapter extends BaseAdapter<TextMessageItem> {


    public ChatAdapter() {
        populateDelegateAdapters();
    }

    @Override
    void populateDelegateAdapters() {
        delegateAdapters.append(AdapterKeys.ME_MESSAGE_TYPE, new MeMessageDelegateAdapter());
        delegateAdapters.append(AdapterKeys.OTHER_MESSAGE_TYPE, new OtherMessageDelegateAdapter());
    }
}
