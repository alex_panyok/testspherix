package testchat.panok.com.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;

import testchat.panok.com.R;
import testchat.panok.com.chat.ChatService;
import testchat.panok.com.chat.callback.ChatServiceMessageCallback;
import testchat.panok.com.enums.SideType;
import testchat.panok.com.model.TextMessage;
import testchat.panok.com.model.adapter.MeTextMessageItem;
import testchat.panok.com.model.adapter.OtherTextMessageItem;
import testchat.panok.com.view.adapter.ChatAdapter;

final public class ChatActivity extends BaseActivity implements View.OnClickListener, ChatServiceMessageCallback {

    private AppCompatEditText etMessage;

    private ChatAdapter chatAdapter;

    private View bSend;

    private RecyclerView recyclerViewChat;

    private TextWatcher etChatTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            bSend.setEnabled(editable.length() > 0);
            bSend.setClickable(editable.length() > 0);
        }
    };

    public static void start(SideType sideType, Context context) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(SIDE_TYPE, sideType);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SideType sideType = (SideType) getIntent().getSerializableExtra(SIDE_TYPE);
        startChatServiceWithAction(sideType == SideType.CLIENT ? ChatService.ACTION_CLIENT : ChatService.ACTION_SERVER, null);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initViews() {
        etMessage = findViewById(R.id.etMessage);
        etMessage.addTextChangedListener(etChatTextWatcher);
        bSend = findViewById(R.id.bSend);
        bSend.setOnClickListener(this);
        bSend.setEnabled(false);
        bSend.setClickable(false);
        initRv();
    }

    private void initRv() {
        recyclerViewChat = findViewById(R.id.rvChat);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        recyclerViewChat.setLayoutManager(linearLayoutManager);
        chatAdapter = new ChatAdapter();
        chatAdapter.setHasStableIds(true);
        recyclerViewChat.setAdapter(chatAdapter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_chat;
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.bSend)
            chatService.sendTextMessage(etMessage.getText().toString());
    }

    @Override
    protected void onDestroy() {
        chatService.disconnect();
        super.onDestroy();
    }

    @Override
    public void disconnect() {
        super.disconnect();
        finish();
    }

    private void scrollRv() {
        recyclerViewChat.post(new Runnable() {
            @Override
            public void run() {
                recyclerViewChat.smoothScrollToPosition(chatAdapter.getItemCount() - 1);
            }
        });
    }

    @Override
    public void textMessage(TextMessage message) {
        chatAdapter.addItem(new OtherTextMessageItem(message));
        scrollRv();
    }

    @Override
    public void meTextMessage(TextMessage message) {
        chatAdapter.addItem(new MeTextMessageItem(message));
        scrollRv();
        etMessage.setText("");
    }
}
