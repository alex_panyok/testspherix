package testchat.panok.com.view;

import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import testchat.panok.com.R;
import testchat.panok.com.chat.ChatService;
import testchat.panok.com.chat.callback.ChatServiceJoinCallback;
import testchat.panok.com.enums.State;
import testchat.panok.com.model.Address;
import testchat.panok.com.model.JoinMessage;
import testchat.panok.com.utils.ConnectivityUtils;

final public class MainActivity extends BaseActivity implements View.OnClickListener, ChatServiceJoinCallback {

    private Button bToggleServer;

    private Button bToggleClient;

    private TextView tvIp;

    private EditText etIp;

    @Override
    protected void initViews() {
        bToggleServer = findViewById(R.id.bToggleServer);
        bToggleClient = findViewById(R.id.bToggleClient);
        tvIp = findViewById(R.id.tvIp);
        etIp = findViewById(R.id.etIp);
        bToggleServer.setOnClickListener(this);
        bToggleClient.setOnClickListener(this);
        tvIp.setText(ConnectivityUtils.getIp(this));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bToggleServer:
                bToggleServer.setText(getString(R.string.processing));
                setEnabledServerUi(false);
                setEnabledClientUi(false);
                startChatServiceWithAction(ChatService.ACTION_SERVER, null);
                break;

            case R.id.bToggleClient:
                bToggleClient.setText(getString(R.string.processing));
                setEnabledClientUi(false);
                setEnabledServerUi(false);
                startChatServiceWithAction(ChatService.ACTION_CLIENT, new Address(etIp.getText().toString(), ChatService.PORT));
                break;

        }
    }

    @Override
    public void join(JoinMessage joinMessage) {
        if (joinMessage.getStatus() == JoinMessage.JOINED) {
            ChatActivity.start(joinMessage.getSideType(), this);
        } else if (joinMessage.getStatus() == JoinMessage.REJECTED) {
            disconnect();
        }
    }

    private void setEnabledClientUi(boolean enabled) {
        bToggleClient.setEnabled(enabled);
        etIp.setEnabled(enabled);
    }

    private void setEnabledServerUi(boolean enabled) {
        bToggleServer.setEnabled(enabled);
    }


    @Override
    public void serverStateChanged(State serverState) {
        switch (serverState) {
            case STARTED:
                tvIp.setTextColor(Color.GREEN);
                setEnabledClientUi(false);
                setEnabledServerUi(true);
                bToggleServer.setText(getString(R.string.stop));
                break;
            case OFF:
                tvIp.setTextColor(Color.BLACK);
                setEnabledClientUi(true);
                setEnabledServerUi(true);
                bToggleServer.setText(getString(R.string.start));
                break;
        }
    }

    @Override
    public void clientChangeState(State clientState) {
        switch (clientState) {
            case STARTED:
                bToggleClient.setText(getString(R.string.stop));
                setEnabledClientUi(true);
                etIp.setEnabled(false);
                setEnabledServerUi(false);
                break;
            case OFF:
                bToggleClient.setText(getString(R.string.start));
                setEnabledServerUi(true);
                setEnabledClientUi(true);
                break;
        }
    }


}
