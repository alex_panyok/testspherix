package testchat.panok.com.view.adapter.delegate;

import android.view.ViewGroup;

import testchat.panok.com.model.adapter.OtherTextMessageItem;
import testchat.panok.com.view.adapter.delegate.holder.OtherTextMessageHolder;

final public class OtherMessageDelegateAdapter extends TextMessageDelegateAdapter<OtherTextMessageItem, OtherTextMessageHolder> {

    @Override
    public OtherTextMessageHolder onCreateViewHolder(ViewGroup parent) {
        return new OtherTextMessageHolder(parent);
    }
}
