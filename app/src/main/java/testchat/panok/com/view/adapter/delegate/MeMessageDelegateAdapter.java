package testchat.panok.com.view.adapter.delegate;

import android.view.ViewGroup;

import testchat.panok.com.model.adapter.MeTextMessageItem;
import testchat.panok.com.view.adapter.delegate.holder.MeTextMessageHolder;

final public class MeMessageDelegateAdapter extends TextMessageDelegateAdapter<MeTextMessageItem, MeTextMessageHolder> {

    @Override
    public MeTextMessageHolder onCreateViewHolder(ViewGroup parent) {
        return new MeTextMessageHolder(parent);
    }
}
