package testchat.panok.com.view;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import testchat.panok.com.chat.ChatService;
import testchat.panok.com.chat.ChatServiceImpl;
import testchat.panok.com.chat.callback.ChatServiceCallback;
import testchat.panok.com.model.Address;

public abstract class BaseActivity extends AppCompatActivity implements ChatServiceCallback {

    public static final String SIDE_TYPE = "SIDE_TYPE";

    protected ChatService chatService;

    protected boolean chatServiceBound = false;

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            ChatServiceImpl.ChatBinder chatBinder = (ChatServiceImpl.ChatBinder) iBinder;
            chatService = chatBinder.getService();
            chatService.addCallback(BaseActivity.this);
            chatServiceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            chatService.removeCallback(BaseActivity.this);
            chatServiceBound = false;
        }

        @Override
        public void onBindingDied(ComponentName name) {
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        initViews();
    }


    protected abstract int getLayoutId();

    protected abstract void initViews();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService();
    }

    protected void unbindService(){
        if(chatService!=null)
            chatService.removeCallback(BaseActivity.this);
        if(chatServiceBound)
            unbindService(connection);
    }

    protected void stopService(){
        unbindService();
        stopService(new Intent(this, ChatServiceImpl.class));
    }

    protected void startChatServiceWithAction(String action, Address address) {
        Intent intent = new Intent(this, ChatServiceImpl.class);
        intent.setAction(action);
        intent.putExtra(ChatServiceImpl.ADDRESS, address);
        startService(intent);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void disconnect() {}
}


