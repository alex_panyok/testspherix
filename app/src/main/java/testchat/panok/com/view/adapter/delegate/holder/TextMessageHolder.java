package testchat.panok.com.view.adapter.delegate.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import testchat.panok.com.R;
import testchat.panok.com.model.adapter.TextMessageItem;

public abstract class TextMessageHolder extends RecyclerView.ViewHolder {

    private TextView textViewMessage;

    TextMessageHolder(ViewGroup parent, int layoutId) {
        super(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
        textViewMessage = itemView.findViewById(R.id.tvMessage);
    }

    public void bind(TextMessageItem messageItem) {
        textViewMessage.setText(messageItem.getTextMessage().getText());
    }

}
