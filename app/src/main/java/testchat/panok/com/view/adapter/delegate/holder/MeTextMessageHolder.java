package testchat.panok.com.view.adapter.delegate.holder;

import android.view.ViewGroup;

import testchat.panok.com.R;

final public class MeTextMessageHolder extends TextMessageHolder {

    public MeTextMessageHolder(ViewGroup parent) {
        super(parent, R.layout.item_me_message);
    }
}
